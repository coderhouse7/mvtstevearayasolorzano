# MVTSteveArayaSolorzano

## NUESTRO PRIMER MVT.
Crear una web que permita ver los datos de algunos de tus familiares, guardados en un BD.


## Consigna: 

Crear una web que permite ver los datos de algunos de tus familiares, guardados en un BD.

1. Deberá tener un template, una vista y un modelo (como mínimo, pueden usar más).
2. La clase del modelo, deberá guardar mínimo un número, una cadena y una fecha (puede
guardar más cosas).
3. Se deberán crear como mínimo 3 familiares.
4. Los familiares se deben ver desde la web.



## Crear Familiar: 

![Screenshot](./Evidencias/Crear_Familiar.png)

## Ver Familiares: 

![Screenshot](./Evidencias/Ver_Familiares.png)