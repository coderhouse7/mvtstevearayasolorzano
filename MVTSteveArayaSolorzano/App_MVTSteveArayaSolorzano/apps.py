from django.apps import AppConfig


class AppMvtstevearayasolorzanoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'App_MVTSteveArayaSolorzano'
