from django.urls import path
from App_MVTSteveArayaSolorzano.views import home, family_list, family

urlpatterns = [    
    path('', home),
    path('add-family/<name>/<last_name>/<birthday>/<relationship>/<age>', family),
    path('family-list/', family_list),
]