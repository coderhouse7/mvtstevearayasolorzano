from django.http import HttpResponse
from django.shortcuts import render
from App_MVTSteveArayaSolorzano.models import Familiares

# Create your views here.

def home(self):
    return render(self, "home.html")

def family(self, name, last_name, birthday, relationship, age):

    family = Familiares(
        name=name, 
        last_name=last_name,
        birthday=birthday, 
        relationship=relationship,
        age=age
    )

    family.save()

    return HttpResponse(f"""
        <p>El familiar: {family.name} {family.last_name} fue creado con exito!!!.</p> 
    """)

def family_list(self):
    list = Familiares.objects.all()
    return render(self, "family_list.html", {"family_list":list})