from django.db import models

# Create your models here.

class Familiares(models.Model):
    name         = models.CharField(max_length=200)
    last_name    = models.CharField(max_length=200)
    birthday     = models.DateField()
    relationship = models.CharField(max_length=200)
    age          = models.IntegerField()
